package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://intelligent-system.online")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    EditText username, password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
    }

    public void onSMTClickd(View v) {
        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText())) {
            SomeAPI api = retrofit.create(SomeAPI.class);
            api.basic(new User(String.valueOf(username.getText()), String.valueOf(password.getText()))).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(MainActivity.this, "Вошёл", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "Error404", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "Поле(я) пустое(ые)", Toast.LENGTH_SHORT).show();
        }
    }
}

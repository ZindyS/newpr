package com.example.myapplication;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SomeAPI {
    @POST("/api/signin")
    Call<User> basic(@Body User u);
}

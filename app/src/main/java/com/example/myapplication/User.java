package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("token")
    private String token;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
